#

CC       = g++ -Wall -O2 
LIBPP    = -L/usr/lib -libpp
CIBPP    = -I/usr/include -I/usr/include/ibpp -DIBPP_LINUX -I/home/akhmetov/usr/local/include
CFUSE    = -DFUSE_USE_VERSION=25 -D_FILE_OFFSET_BITS=64
LFUSE    = -lfuse -L/home/akhmetov/usr/local/lib
TESTSRC  = test
TESTRTD  = test_run

# Добавлено для Debian GNU/Linux.
DESTDIR  = /tmp

all: sqlfs.o sqlite3.o usqlite.o tbytea.o openedfile.o datalayer.o
	$(CC) -Wall -W -O2 -o sqlfs sqlfs.o sqlite3.o usqlite.o tbytea.o openedfile.o datalayer.o $(LFUSE) -lpthread -ldl -Wl,--rpath -Wl,/usr/local/lib

sqlfs.o: sqlfs.cpp sqlfs.h
	$(CC) sqlfs.cpp -c -o sqlfs.o $(CFUSE) -I.

tbytea.o: tbytea.cpp tbytea.h
	$(CC) tbytea.cpp -c -o tbytea.o

openedfile.o: sqlite/openedfile.cpp sqlite/openedfile.h
	$(CC) sqlite/openedfile.cpp -c -o openedfile.o -I. $(CFUSE)

datalayer.o: sqlite/datalayer.cpp sqlite/datalayer.h sqlfs.h
	$(CC) sqlite/datalayer.cpp -c -o datalayer.o -I. $(CFUSE)

#dynamic_list.o: dynamic_list.cpp dynamic_list.h
#	$(CC) dynamic_list.cpp -c -o dynamic_list.o

sqlite3.o: sqlite/sqlite3.c sqlite/sqlite3.h
	gcc sqlite/sqlite3.c -c -o sqlite3.o -DSQLITE_ENABLE_COLUMN_METADATA -DSQLITE_DEFAULT_FILE_PERMISSIONS=0660

usqlite.o: sqlite/usqlite.cpp sqlite/usqlite.h sqlfs.h
	$(CC) sqlite/usqlite.cpp -c -o usqlite.o $(CFUSE) -I.

test: test_usqlite.o sqlite3.o test_sqlfs.o test_tbytea.o test_datalayer.o test_openedfile.o
	$(CC) -Wall -W -O2 -o sqlfstest test_usqlite.o sqlite3.o test_sqlfs.o test_tbytea.o test_datalayer.o test_openedfile.o $(LFUSE) -lpthread -ldl -lgtest -Wl,--rpath -Wl,/usr/local/lib
	./sqlfstest

test_sqlfs.o: sqlfs.cpp sqlfs.h
	$(CC) sqlfs.cpp -c -o test_sqlfs.o $(CFUSE) -I. -DTEST_BUILD=1 -DGTEST_HAS_PTHREAD=0

test_usqlite.o: sqlite/usqlite.cpp sqlite/usqlite.h
	$(CC) sqlite/usqlite.cpp -c -o test_usqlite.o -I. $(CFUSE) -I. -Isqlite/ -DTEST_BUILD=1 -DGTEST_HAS_PTHREAD=0

test_datalayer.o: sqlite/datalayer.cpp sqlite/datalayer.h
	$(CC) sqlite/datalayer.cpp -c -o test_datalayer.o -I. $(CFUSE) -DTEST_BUILD=1 -DGTEST_HAS_PTHREAD=0

test_openedfile.o: sqlite/openedfile.cpp sqlite/openedfile.h
	$(CC) sqlite/openedfile.cpp -c -o test_openedfile.o -I. $(CFUSE) -DTEST_BUILD=1 -DGTEST_HAS_PTHREAD=0

test_tbytea.o: tbytea.cpp tbytea.h
	$(CC) tbytea.cpp -c -o test_tbytea.o -DTEST_BUILD=1 -DGTEST_HAS_PTHREAD=0

clean:
	rm -rf *.o sqlfs sqlfstest _*.cpp
