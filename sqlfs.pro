######################################################################
# Automatically generated by qmake (2.01a) ?? 30. ??? 14:28:34 2018
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += . sqlite
INCLUDEPATH += .

include(sqlite/sqlite.pri)

# Input
HEADERS += sqlfs.h sqlite/usqlite.h \
    tbytea.h \
    sqlite/openedfile.h \
    sqlite/datalayer.h

SOURCES += sqlfs.cpp sqlite/usqlite.cpp \
    tbytea.cpp \
    sqlite/openedfile.cpp \
    sqlite/datalayer.cpp

DEFINES += FUSE_USE_VERSION=25
DEFINES += TEST_BUILD
