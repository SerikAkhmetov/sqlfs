#ifndef TBYTEA_H
#define TBYTEA_H

#include <string>
//---------------------------------------------------------------------------
class TBytea                     // class for BLOB field
{
// friend class TBytea;
 protected:
  int64_t         size;
  int64_t         position;
  unsigned char * data;
  unsigned int    paddedCharacters;
 public:
  TBytea();
  TBytea(const int64_t size);
  TBytea(const TBytea&);
  virtual ~TBytea();
  int64_t       set_size(const int64_t);
  const int64_t get_size(void) const;
  int64_t       set_position(const int64_t);
  const int64_t get_position(void) const;
  void          clear(void);
  void          free(void);
  size_t       read(void *, const size_t size);   // чтение из потока
  int64_t       write(const void *, const int64_t size);  // запись в поток
// оператор =, [], +, <<, >>
  int64_t       load_from_file(const std::string&);
  int64_t       save_to_file(const std::string&) const;
#ifdef BOOST_CONFIG_HPP
  const std::string base64(void) const;
  const std::string sha1(void) const;
#endif
  unsigned char * operator[](const int64_t) const;
  bool operator == (const TBytea n);
  TBytea& operator = (const int64_t& n) {set_size(n); return (*this);};
  TBytea& operator = (const TBytea& r);
  TBytea& operator = (const std::string& r);
};
//---------------------------------------------------------------------------
#endif // TBYTEA_H
