#include <cstring>
#include <iostream>
#include <fstream>

#ifdef TEST_BUILD
#include <cstring>
#include <gtest/gtest.h>
#endif

#include "tbytea.h"

TBytea::TBytea():
 size(0), position(0)
{
 data     = 0;
}
//---------------------------------------------------------------------------
TBytea::TBytea(const int64_t new_size):size(0), position(0)
{
 set_size(new_size);
}
//---------------------------------------------------------------------------
TBytea::TBytea(const TBytea& orig):
 size(0),
 position(orig.position)
{
 set_size(orig.get_size());
 memcpy(data, orig.data, size);
}
//---------------------------------------------------------------------------
TBytea::~TBytea()
{
 if (data != 0)
 {
  delete [] data;
 }
}
//---------------------------------------------------------------------------
int64_t TBytea::set_size(const int64_t new_size)
{
 unsigned char * new_data, * old_data;

 paddedCharacters = new_size % 3;

 if (new_size == 0)
 {
  free();
  return (0);
 }

 if (size == 0 && new_size > 0)
 {
  data = new unsigned char [new_size + paddedCharacters];
  size = new_size;
 }
 else
 {
  new_data = new unsigned char [new_size + paddedCharacters];
  old_data = data;
  memcpy(new_data, old_data, std::min(new_size, size));
  size = new_size;
  data = new_data;
  delete [] old_data;
 }

 if (paddedCharacters > 0)
 {
  memset(data + size, 0, paddedCharacters);
 }

 return (size);
}
//---------------------------------------------------------------------------
const int64_t TBytea::get_size(void) const
{
 return (size);
}
//---------------------------------------------------------------------------
int64_t TBytea::set_position(const int64_t new_pos)
{
 if (new_pos <= size)
  return (position = new_pos);
 else
  return (position = size);
}
//---------------------------------------------------------------------------
const int64_t TBytea::get_position(void) const
{
 return (position);
}
//---------------------------------------------------------------------------
void TBytea::clear(void)
{
 memset(data, 0, size);
 return ;
}
//---------------------------------------------------------------------------
void TBytea::free(void)
{
 if (data != NULL)
   delete [] data;
 data     = NULL;
 size     = 0;
 position = 0;
 return;
}
//---------------------------------------------------------------------------
// read from stream
size_t TBytea::read(void * dst, const size_t buf_size)
{
 size_t ret(buf_size);

 if ((position + buf_size) > size) ret = size - position;

 memcpy(dst, &data[position], ret);

// int v(0);
// memcpy(&v, &data[position], sizeof(int));
// std::cout << " TBytea::read v=" << v;

 position +=ret;

 return (ret);
}
//---------------------------------------------------------------------------
// write to stream
int64_t TBytea::write(const void * src, const int64_t buf_size)
{
 int64_t ret(buf_size);

 if (buf_size > (size - position)) set_size(position + buf_size);

 memcpy(&data[position], src, ret);

// int v(0);
// memcpy(&v, &data[position], sizeof(int));
// std::cout << " TBytea::write v=" << v;

 position += ret;

 return (ret);
}
//---------------------------------------------------------------------------
int64_t TBytea::load_from_file(const std::string& file_name)
{
 int64_t ret(0), new_size(0);
 std::ifstream fin;

 fin.open(file_name.c_str(), std::ios::binary);
 if (fin.fail())
 {
  std::cout << "Error open file " << file_name << std::endl;
  return(-1);
 }

 if (fin.seekg(0, std::ios::end))
 {
  new_size = fin.tellg();
 }
 else
 {
  // TODO: Error
 }

 set_size(new_size);

 fin.seekg(0);
 fin.read((char *)data, size);
 if (fin.fail())
 {
  std::cout << "Error read file " << file_name << std::endl;
  return(-1);
 }

 fin.close();

 position = 0;
 ret      = size;

 return (ret);
}
//---------------------------------------------------------------------------
int64_t TBytea::save_to_file(const std::string& file_name) const
{
 int64_t ret(0);
 std::ofstream fout;

 fout.open(file_name.c_str(), std::ios::binary);
 if (fout.fail())
 {
  std::cout << "Error open file " << file_name << std::endl;
  return (-1);
 }

 const int chunk_size = 16 * 1024;
 for(int64_t i = 0; i < size; i += chunk_size)
 {
  fout.write((char *)data + i, i + chunk_size < size ? chunk_size : size - i);
 }
 fout.close();

 ret = size;
 return (ret);
}
//---------------------------------------------------------------------------
#ifdef BOOST_CONFIG_HPP
const std::string TBytea::base64(void) const
{
 typedef boost::archive::iterators::base64_from_binary
         <boost::archive::iterators::transform_width<unsigned char *, 6, 8> > base64Iterator;

 std::string encodedString(
      base64Iterator(data),
      base64Iterator(data + size));

 // Add '=' for each padded character used
 for(unsigned int i = 0; i < paddedCharacters; i++)
 {
  encodedString.push_back('=');
 }
 return encodedString;
}
//---------------------------------------------------------------------------
const std::string TBytea::sha1(void) const
{
 boost::uuids::detail::sha1 sha1;
 sha1.reset();
 sha1.process_bytes(data, size);
 return(sha12str(sha1));
}
#endif
//---------------------------------------------------------------------------
unsigned char * TBytea::operator[](const int64_t idx) const
{
 unsigned char * ret = NULL;

 if (idx > (size-1) || idx < 0)
 {
  // TODO : Error
 }
 else
 {
  ret = data + idx;
 }

 return(ret);
}
//---------------------------------------------------------------------------
bool TBytea::operator == (const TBytea n)
{
 bool ret = false;

 if (n.size != size) return(ret);

 if (size == 0) return(true);

 for (int64_t i = 0; i < size; i++)
 {
  if (data[i] != n.data[i]) break;
 }
 return (ret);
}
//---------------------------------------------------------------------------
TBytea& TBytea::operator = (const TBytea& r)
{
 set_size(r.get_size());
 memcpy(data, &r.data[0], size);
 position = 0;
 return (*this);
}
//---------------------------------------------------------------------------
TBytea& TBytea::operator = (const std::string& r)
{
 set_size(r.length());
 memcpy(data, r.c_str(), size);
 position = 0;
 return (*this);
}
//---------------------------------------------------------------------------

#ifdef TEST_BUILD

class TestBytea : public ::testing::Test, public TBytea
{
 public:
  TestBytea() { /* init protected members here */ };
 ~TestBytea() { /* free protected members here */ };
// void SetUp();// { /* called before every test */ }
// void TearDown();// { /* called after every test */ }
};

TEST_F(TestBytea, set_size)
{
 set_size(0);
 ASSERT_EQ(0, size);
 ASSERT_EQ(0, get_size());

 set_size(1024);
 ASSERT_EQ(1024, size);
 ASSERT_EQ(1024, get_size());
}

TEST_F(TestBytea, set_position)
{
 set_size(1024);
 ASSERT_EQ(1024, size);

 set_position(0);
 ASSERT_EQ(0, position);
 ASSERT_EQ(0, get_position());
 set_position(100);
 ASSERT_EQ(100, position);
 ASSERT_EQ(100, get_position());

 set_position(1023);
 ASSERT_EQ(1023, position);
 ASSERT_EQ(1023, get_position());

 set_position(4096);
 ASSERT_EQ(1024, position);
 ASSERT_EQ(1024, get_position());
}

TEST_F(TestBytea, clear)
{
 set_size(1024);
 data[0] = 100;
 data[100] = 150;
 data[1000] = 200;

 ASSERT_EQ(100, data[0]);
 ASSERT_EQ(150, data[100]);
 ASSERT_EQ(200, data[1000]);

 clear();
 for(int i = 0; i < size; i++)
 { ASSERT_EQ(0, data[i]);}
}

TEST_F(TestBytea, free)
{
 set_size(1024);

 free();

 ASSERT_EQ(0, size);
}

TEST_F(TestBytea, read)
{
 set_size(1024);

 for(int i = 0; i +sizeof(i) < get_size(); i += sizeof(i))
 {
  write(&i, sizeof(i));
 }

 set_position(0);
 for(int i = 0; i +sizeof(i) < get_size(); i += sizeof(i))
 {
  int v(0);
  read(&v, sizeof(v));
  ASSERT_EQ(i, v);
 }

 unsigned char buff[1024];
 memset(buff, 0, 1024 * sizeof(unsigned char));
 for(int i = 0; i < 1024; i++)
 { buff[i] = i; }

 clear();
 set_position(0);
 write(buff, 1024);

 memset(buff, 0, 1024 * sizeof(unsigned char));
 set_position(0);
 read(buff, 1024);
 for(int i = 0; i < 1024; i++)
 { ASSERT_EQ((unsigned char)i, buff[i]);}
}

TEST_F(TestBytea, write)
{
 set_size(1024);

 for(int i = 0; i +sizeof(i) < get_size(); i += sizeof(i))
 {
  write(&i, sizeof(i));
 }

 unsigned char buff[1024];
 memset(buff, 0, 1024 * sizeof(unsigned char));
 for(int i = 0; i < 1024; i++)
 { buff[i] = (unsigned char)i; }

 clear();
 set_position(0);
 write(buff, 1024);
}

#endif
