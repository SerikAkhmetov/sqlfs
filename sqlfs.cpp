#include <iostream>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>

#include <cstring>
#include <unistd.h>

#include "sqlfs.h"
#include "sqlite/usqlite.h"

#ifdef TEST_BUILD
#include <gtest/gtest.h>
#include <pthread.h>
#endif

static TSQLfs sqlfs;

//-----------------------------------------------------------------------

int main(int argc, char *argv[])
{
#ifndef TEST_BUILD

 std::cout << "sqlfs" << std::endl;

 TMeta * meta = MetaSQLiteFactory("/home/serik/src/sqlfs/sqlite/sqlfs.db");
 TContent * content = ContentSQLiteFactory(meta);
 sqlfs.add("/", meta, content);
 sqlfs.start(argv[0], "/home/serik/var/pfs");

#else
 testing::InitGoogleTest(&argc, argv);
 return RUN_ALL_TESTS();
#endif
}
//-----------------------------------------------------------------------
static int fgetattr(const char *path, struct stat *stbuf)
{
 return(sqlfs.fgetattr(path, stbuf));
}
/*static int freadlink(const char *, char *, size_t)
{}*/
static int fmknod(const char *path, mode_t mode, dev_t dev)
{
 return(sqlfs.fmknod(path, mode, dev));
}
static int fmkdir(const char *path, mode_t mode)
{
 return(sqlfs.fmkdir(path, mode));
}
static int funlink(const char *path)
{
 return(sqlfs.funlink(path));
}
static int frmdir(const char *path)
{
  return(sqlfs.frmdir(path));
}
/*static int fsymlink(const char *, const char *)
{}
static int frename(const char *, const char *, unsigned int flags)
{}
static int flink(const char *, const char *)
{}*/
static int fchmod(const char * path, mode_t mode)
{
 return(sqlfs.fchmod(path, mode));
}
/*static int fchown(const char *, uid_t, gid_t, struct fuse_file_info *fi)
{}*/
static int ftruncate(const char * path, off_t size)
{
 return(sqlfs.ftruncate(path, size));
}
static int fopen(const char * path, fuse_file_info * fi)
{
 return(sqlfs.fopen(path, fi));
}
static int fread(const char *path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi)
{
 return(sqlfs.fread(path, buf, size, offset, fi));
}
static int fwrite(const char *path, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi)
{
 return(sqlfs.fwrite(path, buf, size, offset, fi));
}
/*static int fstatfs(const char *, struct statvfs *)
{}
static int fflush(const char *, struct fuse_file_info *)
{}*/
static int frelease(const char * path, struct fuse_file_info * fi)
{
 return(sqlfs.frelease(path, fi));
}
/*static int ffsync(const char *, int, struct fuse_file_info *)
{}
static int fsetxattr(const char *, const char *, const char *, size_t, int)
{}
static int fgetxattr(const char *, const char *, char *, size_t)
{}
static int flistxattr(const char *, char *, size_t)
{}
static int fremovexattr(const char *, const char *)
{}
static int fopendir(const char *, struct fuse_file_info *)
{}*/
static int freaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
 return(sqlfs.freaddir(path, buf, filler, offset, fi));
}
/*static int freleasedir(const char *, struct fuse_file_info *)
{}
static int ffsyncdir(const char *, int, struct fuse_file_info *)
{}
static void * finit(struct fuse_conn_info *conn, struct fuse_config *cfg)
{}
static void fdestroy(void *private_data)
{}
static int faccess(const char *, int)
{}
static int fcreate(const char *, mode_t, struct fuse_file_info *)
{}
static int flock(const char *, struct fuse_file_info *, int cmd, struct flock *)
{}
static int futimens(const char *, const struct timespec tv[2], struct fuse_file_info *fi)
{}
static int fbmap(const char *, size_t blocksize, uint64_t *idx)
{}
static int fioctl(const char *, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data)
{}
static int fpoll(const char *, struct fuse_file_info *, struct fuse_pollhandle *ph, unsigned *reventsp)
{}
static int fwrite_buf(const char *, struct fuse_bufvec *buf, off_t off, struct fuse_file_info *)
{}
static int fread_buf(const char *, struct fuse_bufvec **bufp, size_t size, off_t off, struct fuse_file_info *)
{}
static int fflock(const char *, struct fuse_file_info *, int op)
{}
static int ffallocate(const char *, int, off_t, off_t, struct fuse_file_info *)
{}*/
//-----------------------------------------------------------------------
TFileAttr::TFileAttr()
{
 clear();
}

void TFileAttr::clear()
{
 parent = 0;
 file_name.clear();
 memset(&st, 0, sizeof(struct stat));
}

void TFileAttr::copyTo(struct stat * dest)
{
 memcpy(dest, &st, sizeof(struct stat));
}
//-----------------------------------------------------------------------
/*TFileContent::TFileContent(const int size)
{
 ba = new TBytea(size);
}

TFileContent::~TFileContent()
{
 delete(ba);
}*/
//-----------------------------------------------------------------------
TSQLfs::TSQLfs()
{
 fuse_op.getattr = ::fgetattr;
 fuse_op.readdir = ::freaddir;
 fuse_op.mkdir = ::fmkdir;
 fuse_op.rmdir = ::frmdir;
 fuse_op.open = ::fopen;
 fuse_op.truncate = ::ftruncate;
 fuse_op.unlink = ::funlink;
 fuse_op.release = ::frelease;
 fuse_op.read = ::fread;
 fuse_op.write = ::fwrite;
 fuse_op.mknod = ::fmknod;
 fuse_op.chmod = ::fchmod;
}

void TSQLfs::add(const std::string &root, TMeta * pmeta, TContent * pcontent)
{
 TMetaContent * m = new TMetaContent();
 m->root = root;
 m->meta = pmeta;
 m->content = pcontent;
 lst_fs.push_back(m);
}

int TSQLfs::start(const std::string argv0, const std::string mount_point)
{
  int ret;

  char **fargv;

  fargv = new char * [6];
  fargv[0] = strdup(argv0.c_str());
  fargv[1] = strdup("-o");
  fargv[2] = strdup("fsname=sqlFS");
  fargv[3] = strdup(mount_point.c_str());
  fargv[4] = strdup("-o");
  fargv[5] = strdup("allow_other");

  struct fuse_args args = FUSE_ARGS_INIT (6, fargv);
#if FUSE_USE_VERSION == 25
   ret = ::fuse_main(args.argc , args.argv , &fuse_op);
//#else
//   ret = ::fuse_main(args.argc , args.argv , &fuse_op, this);
#endif
 //  if (ret != 0)
   {
 //    printf("sqlfs start is failed\n");
     return (ret);
   }
 //  else return (0);
}

int TSQLfs::fgetattr(const char *path, struct stat *stbuf)
{
 memset(stbuf, 0, sizeof(struct stat));
 if(strcmp(path, "/") == 0)    //
 {
  tm t={15,10,10,1,5,106,0,0,0,0,NULL};
  time_t tt = mktime(&t);
  stbuf->st_ino   = 0;

     //if (BUILD_READ_ONLY) {stbuf->st_mode  = S_IFDIR | 0755;}
     //else
     {stbuf->st_mode  = S_IFDIR | 0777;}
     stbuf->st_nlink = 1;
     stbuf->st_size  = 1024;
     stbuf->st_blksize = 4096;
     stbuf->st_atime = tt; //time(NULL);
     stbuf->st_mtime = tt; //time(NULL);
     stbuf->st_ctime = tt; //time(NULL);
     return (0);
    }
   { // check for BAD path /bla/bla/*
    std::string dir, name;
    if (parse_path(path, dir, name))
    {
     if (name == "*") return (-ENOENT);
    }
   }

 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  TFileAttr attr;
  int r = (*itr)->meta->getattr(std::string(path), attr);
  if (r == 0)
    attr.copyTo(stbuf);
//  if (r == -1)
//    return(-errno);
//  else
  return(r);
 }
 return(-ENOENT);
}

int TSQLfs::freaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset,
                     struct fuse_file_info *fi)
{

 if(strlen(path) > 1)
 {
  filler(buf, ".", NULL, 0);
  filler(buf, "..", NULL, 0);
 }

 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  TFileList lst;
  TFileList::iterator i2;
  int r = (*itr)->meta->readdir(std::string(path), lst, offset, fi);
  for(i2 = lst.begin(); i2 != lst.end(); ++i2)
  {
   struct stat st;
   memset(&st, 0, sizeof(st));
   (*i2)->copyTo(&st);
   if (filler(buf, (*i2)->file_name.c_str(), &st, 0))
            break;
   delete(*i2);
  }
  lst.clear();
  return(r);
 }
 return(0);
}

int TSQLfs::fmkdir(const char *path, mode_t mode)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->meta->mkdir(std::string(path), mode);
  return(r);
 }
 return(-ENOENT);
}

int TSQLfs::funlink(const char *path)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->content->unlink(std::string(path));
  return(r);
 }
 return(0);
}

int TSQLfs::frmdir(const char *path)
{
 bool f(true);
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  TFileList lst;
  int r = (*itr)->meta->readdir(std::string(path), lst, 0, NULL);
  for(TFileList::iterator i2 = lst.begin(); i2 != lst.end() && f; ++i2)
  {
   std::string full_name(std::string(path) + std::string("/") + (*i2)->file_name);
   if (f && (*i2)->st.st_mode & S_IFREG)
   {
    f = (*itr)->content->unlink(full_name) == 0;
    continue;
   }
   if (f && (*i2)->st.st_mode & S_IFDIR)
   {
    f = (*itr)->meta->rmdir(full_name) == 0;
    continue;
   }
   f = false;
  }
  for(TFileList::iterator i2 = lst.begin(); i2 != lst.end(); ++i2)
  {delete(*i2);}
  lst.clear();
  if (f)
  {
   return ((*itr)->meta->rmdir(std::string(path)));
  }
  return(-ENOENT);
 }
 return(-ENOENT);
}

int TSQLfs::fmknod(const char *path, mode_t mode, dev_t dev)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->meta->mknod(std::string(path), mode, dev);
  return(r);
 }
 return(0);
}

int TSQLfs::fchmod(const char *path, mode_t mode)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->meta->chmod(std::string(path), mode);
  return(r);
 }
 return(0);
}

int TSQLfs::ftruncate(const char * path, off_t size)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  if ((*itr)->content->truncate(std::string(path), size) == 0)
    return(0);
 }
 return(-ENOENT);
}

int TSQLfs::fopen(const char *path, fuse_file_info * fi)
{

 if (fi->flags & O_EXCL && fi->flags & O_CREAT)
 {
  return(-ENOENT);
 }

 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->content->open(std::string(path), fi);
  if (r == -ENOENT && fi->flags & O_CREAT)
  {
   r = (*itr)->meta->mknod(std::string(path), S_IFREG | fi->flags, NULL);
   if (r == 0)
   {return ((*itr)->content->open(std::string(path), fi));}
  }
  return(r);
 }
 return(0);
}

int TSQLfs::frelease(const char *path, struct fuse_file_info *fi)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->content->release(std::string(path), fi);
  return(r);
 }
 return(0);
}

int TSQLfs::fread(const char *path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->content->read(std::string(path), buf, size, offset, fi);
  return(r);
 }
 return(0);
}

int TSQLfs::fwrite(const char *path, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi)
{
 std::list<TMetaContent *>::iterator itr;
 for(itr = lst_fs.begin(); itr != lst_fs.end(); ++itr)
 {
  int r = (*itr)->content->write(std::string(path), buf, size, offset, fi);
  return(r);
 }
 return(0);
}

//-----------------------------------------------------------------------

int TfsMeta::getattr(const std::string &path, TFileAttr & attr)
{
 return stat(path.c_str(), &attr.st);
}

int TfsMeta::readdir(const std::string &path, TFileList &lst, off_t offset, struct fuse_file_info *fi)
{
  DIR *dp;
  struct dirent *de;

  (void) offset;
  (void) fi;

  dp = opendir(path.c_str());
  if (dp == NULL)
      return -errno;

  while ((de = ::readdir(dp)) != NULL) {
      TFileAttr * attr = new TFileAttr();
      attr->file_name = de->d_name;
      attr->st.st_ino = de->d_ino;
      attr->st.st_mode = de->d_type << 12;

    //  attr->st.st_uid = 1295;
    //  attr->st.st_gid = 1026;
    //  attr->st.st_mode = attr->st.st_mode | 5;

      lst.push_back(attr);
  }

  closedir(dp);
  return 0;
}
int TfsMeta::mkdir(const std::string &path, mode_t mode)
{
 return(::mkdir(path.c_str(), mode));
}

int TfsMeta::rmdir(const std::string &path)
{
 return(::rmdir(path.c_str()));
}

int TfsMeta::mknod(const std::string &path, mode_t mode, dev_t dev)
{
 return(mknod(path.c_str(), mode, dev));
}

int TfsMeta::chmod(const std::string &path, mode_t mode)
{
 return(chmod(path.c_str(), mode));
}
//-----------------------------------------------------------------------
int TfsContent::open(const std::string &path, struct fuse_file_info *fi)
{
 int res, ret(0);

 res = ::open(path.c_str(), fi->flags);
 if (res == -1)
     ret = -errno;
 else
   ::close(res);

 return ret;
}

int TfsContent::release(const std::string &path, struct fuse_file_info *fi)
{
 return 0;
}

int TfsContent::read(const std::string &path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi)
{
  int fd;
  int res;

  (void) fi;
  fd = ::open(path.c_str(), O_RDONLY);
  if (fd == -1)
      return -errno;

  res = ::pread(fd, buf, size, offset);
  if (res == -1)
  {
   res = -errno;
  }

  ::close(fd);
  return res;
}

int TfsContent::write(const std::string &path, const char *buf, size_t size,
                   off_t offset, struct fuse_file_info *fi)
{
 int fd;
 int res;

 (void) fi;
 fd = ::open(path.c_str(), O_WRONLY);
 if (fd == -1)
 {
  return (-errno);
 }

 res = ::pwrite(fd, buf, size, offset);
 if (res == -1)
 {
  res = -errno;
 }

 ::close(fd);
 return res;
}

int TfsContent::truncate(const std::string &full_name, off_t size)
{
 return ::truncate(full_name.c_str(), size);
}

int TfsContent::unlink(const std::string &full_name)
{
 return ::unlink(full_name.c_str());
}
//-----------------------------------------------------------------------
const int split_str(std::vector<std::string> & vct, const std::string &str, const std::string &separator)
{
 unsigned int x(0), y(0);

 while(x <= str.length())
 {
  y = str.find(separator, x);
  if (y != std::string::npos)
  {
   if (str.substr(x, y - x - separator.length() + 1).length() > 0)
     vct.push_back(str.substr(x, y - x - separator.length() + 1));
  }
  else
  {
   if (str.substr(x).length() > 0)
     vct.push_back(str.substr(x));
   break;
  }
  x = y + separator.length();
 }

 return(vct.size());
}

//-----------------------------------------------------------------------
const int parse_path(const std::string &full_path, std::string &path, std::string & name)
{
 int ret(0);
 std::string::size_type x, l;
 const std::string separator ="/";

 path.clear();
 name.clear();

// /hello/word
// /hello/
// /hello
// hello
// /

 l = full_path.length();
 if (l == 0) return (ret);
 x = full_path.rfind(separator);

 if (x == std::string::npos) // separator not found
 {
  name.assign(full_path);
 }
 else
 {
//  path = full_path->substr(0, x);
//  name = full_path->substr(x + 1, l - x - 1);
  path.assign(full_path, 0, x + 1);
  name.assign(full_path, x + 1, l - x - 1);
  ret = 1;
 }

 return (ret);
}
//-----------------------------------------------------------------------

#ifdef TEST_BUILD

class TestSQLfs : public ::testing::Test
{
 public:
  TestSQLfs();
 ~TestSQLfs();
 protected:
  TSQLfs sqlfs; TMeta * meta; TContent * content;
};

TestSQLfs::TestSQLfs()
{
 std::remove("test_run/testdb.db");
 meta = MetaSQLiteFactory("test_run/testdb.db");
 content = ContentSQLiteFactory(meta);
 sqlfs.add("/", meta, content);
 //sqlfs.start(argv[0], "/home/serik/var/pfs");
}

TestSQLfs::~TestSQLfs()
{
 delete(content);
 delete(meta);
 std::remove("test_run/testdb.db");
}

TEST_F(TestSQLfs, frmdir)
{
 int ret = sqlfs.fmkdir("/test_dir", S_IFDIR | 0777);
 ASSERT_EQ(0, ret);

 ret = sqlfs.fmknod("/test_dir/test_node", S_IFREG | 0777, NULL);
 ASSERT_EQ(0, ret);

 ret = sqlfs.frmdir("/test_dir");
 ASSERT_EQ(0, ret);// << "" << std::endl;

 struct stat st;
 ret = sqlfs.fgetattr("/test_dir", &st);
 ASSERT_TRUE(ret < 0);
}

TEST_F(TestSQLfs, fchmod)
{
 int ret = sqlfs.fmkdir("/test_dir", S_IFDIR | 0777);
 ASSERT_EQ(0, ret);

 ret = sqlfs.fmknod("/test_dir/test_node", S_IFREG | 0777, NULL);
 ASSERT_EQ(0, ret);

 ret = sqlfs.fchmod("/test_dir/test_node", S_IFREG | 0770);
 ASSERT_EQ(0, ret);// << "" << std::endl;

 struct stat st; memset(&st, 0, sizeof(struct stat));
 ret = sqlfs.fgetattr("/test_dir/test_node", &st);
 ASSERT_EQ(0, ret);
 ASSERT_EQ(S_IFREG | 0770, st.st_mode);

 ret = sqlfs.fchmod("/test_dir", S_IFDIR | 0770);
 ASSERT_EQ(0, ret);// << "" << std::endl;

 memset(&st, 0, sizeof(struct stat));
 ret = sqlfs.fgetattr("/test_dir", &st);
 ASSERT_EQ(0, ret);
 ASSERT_EQ(S_IFDIR | 0770, st.st_mode);
}

/*
virtual int fgetattr(const char *path, struct stat *stbuf);
virtual int freaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
virtual int fmkdir(const char *path, mode_t mode);
virtual int funlink(const char *path);
virtual int frmdir(const char *path);
virtual int fmknod(const char *path, mode_t mode, dev_t dev);
virtual int ftruncate(const char *, off_t);
virtual int fopen(const char *path, fuse_file_info * fi);
virtual int frelease(const char *path, struct fuse_file_info *fi);
virtual int fread(const char *path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi);
virtual int fwrite(const char *path, const char *buf, size_t size,
                   off_t offset, struct fuse_file_info *fi);*/

class TestFunc : public ::testing::Test
{
 public:
  TestFunc() { /* init protected members here */ }
 ~TestFunc() { /* free protected members here */ }
// void SetUp() { /* called before every test */ }
// void TearDown() { /* called after every test */ }
// protected:
};

TEST_F(TestFunc, split_str)
{
 std::vector<std::string> vct;
 int ret;

 ret = split_str(vct, "/");
 ASSERT_EQ(0, ret);
 ASSERT_EQ(0, vct.size());

 vct.clear();
 ret = split_str(vct, "/home");
 ASSERT_EQ(1, ret) << "ret false for /home";
 ASSERT_EQ(1, vct.size());
 ASSERT_EQ("home", vct.back());

 vct.clear();
 ret = split_str(vct, "/home/");
 ASSERT_EQ(1, ret) << "ret false for /home/";
 ASSERT_EQ(1, vct.size());
 ASSERT_EQ("home", vct.back());

 vct.clear();
 ret = split_str(vct, "/home/user");
 ASSERT_EQ(2, ret) << "ret false for /home/user";
 ASSERT_EQ(2, vct.size());
 ASSERT_EQ("home", vct.front());
 ASSERT_EQ("user", vct.back());

 vct.clear();
 ret = split_str(vct, "/home/user/");
 ASSERT_EQ(2, ret) << "ret false for /home/user/";
 ASSERT_EQ(2, vct.size());
 ASSERT_EQ("home", vct.front());
 ASSERT_EQ("user", vct.back());

 vct.clear();
 ret = split_str(vct, "/home/user/prg");
 ASSERT_EQ(3, ret) << "ret false for /home/user/prg";
 ASSERT_EQ(3, vct.size());
 ASSERT_EQ("home", vct.front());
 ASSERT_EQ("prg", vct.back());

 vct.clear();
 ret = split_str(vct, "/home/user/prg/");
 ASSERT_EQ(3, ret) << "ret false for /home/user/prg/";
 ASSERT_EQ(3, vct.size());
 ASSERT_EQ("home", vct.front());
 ASSERT_EQ("prg", vct.back());
}

TEST_F(TestFunc, parse_path)
{
 int ret;
 std::string path, name;

 ret = parse_path("", path, name);
 ASSERT_EQ(0, ret) << "ret false for ";

 ret = parse_path("empty", path, name);
 ASSERT_EQ(0, ret) << "ret false for empty";

 ret = parse_path("/", path, name);
 ASSERT_EQ(1, ret) << "ret false for /";
 ASSERT_EQ("/", path);
 ASSERT_EQ("", name);

 ret = parse_path("/home", path, name);
 ASSERT_EQ(1, ret) << "ret false for /home";
 ASSERT_EQ("/", path);
 ASSERT_EQ("home", name);

 ret = parse_path("/home/", path, name);
 ASSERT_EQ(1, ret) << "ret false for /home/";
 ASSERT_EQ("/home/", path);
 ASSERT_EQ("", name);

 ret = parse_path("/home/user", path, name);
 ASSERT_EQ(1, ret) << "ret false for /home/user";
 ASSERT_EQ("/home/", path);
 ASSERT_EQ("user", name);

 ret = parse_path("/home/user/", path, name);
 ASSERT_EQ(1, ret) << "ret false for /home/user/";
 ASSERT_EQ("/home/user/", path);
 ASSERT_EQ("", name);
}

/*class TestFuse : public ::testing::Test
{
 public:
  TestFuse();
 ~TestFuse();
// void SetUp() {  }
// void TearDown() {  }

 protected:
  pthread_t thread_id;
  int start();
  int stop();

 private:
  static int i;
};

static void * FuseThread (void * p)
{
// TMeta * meta = MetaSQLiteFactory("/home/serik/src/sqlfs/sqlite/sqlfs.db");
// TContent * content = ContentSQLiteFactory(meta);
// sqlfs.add("/", meta, content);
// sqlfs.start("sqlfstest", "/home/serik/var/pfs");
// pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
}

TestFuse::TestFuse()
{
 start();
}
TestFuse::~TestFuse()
{
 stop();
}
typedef  void * ( *TPThreadFunc) (void *);
int TestFuse::start()
{
 TPThreadFunc tfuncptr;
 tfuncptr = FuseThread;
 //pthread_create(&thread_id, NULL, tfuncptr, NULL);
 //FuseThread(NULL);
 return(0);
}
int TestFuse::stop()
{
 //pthread_cancel(thread_id);
 return(0);
}

TEST_F(TestFuse, FuseRun)
{
 std::cout << "fuse run" << std::endl;
}
*/
#endif

