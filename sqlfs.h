#ifndef SQLFS_H
#define SQLFS_H

#include <string>
#include <list>
#include <vector>

#include <fuse.h>
#include <sys/stat.h>

#include "tbytea.h"

class TFileAttr
{
 public:
  TFileAttr();
  virtual ~TFileAttr(){};

  int parent;
  std::string file_name;
  struct stat st;

  virtual void clear();
  virtual void copyTo(struct stat * dest);
};

typedef std::list<TFileAttr *> TFileList;

/*class TFileContent
{
 public:
  TFileContent(const int size);
  virtual ~TFileContent();

  int file_id;
  int node_id;
  int off;
  TBytea * ba;
};*/

class TNode: public TBytea
{
public:
 TNode(const int64_t psize): TBytea (psize), node_id(0), off(0),
                             usize(0), modified(false){};

 int node_id;
 int64_t off;
 int64_t usize;
 bool modified;

 /*int64_t write(const void * data, const int64_t size)
 {
  int64_t ret = TBytea::write(data, size);
  if (ret > 0)
    usize = get_position();
  return (ret);
 };*/
};
typedef std::list<TNode *> TNodeList;

class TMeta
{
 public:
  virtual ~TMeta(){};

  virtual int getattr(const std::string &path, TFileAttr & attr) = 0;
//  virtual int access(const std::string &path, int mask) = 0;
  virtual int readdir(const std::string &path, TFileList &lst, off_t offset = 0, struct fuse_file_info *fi = NULL) = 0;
  virtual int mkdir(const std::string &path, mode_t mode) = 0;
  virtual int rmdir(const std::string &path) = 0;
  virtual int mknod(const std::string &path, mode_t mode, dev_t dev) = 0;
  virtual int chmod(const std::string &path, mode_t mode) = 0;
//  virtual bool getAttr
};


class TfsMeta : public TMeta
{
 public:

  int getattr(const std::string &path, TFileAttr & attr);
//  virtual int access(const std::string &path, int mask);
  int readdir(const std::string &path, TFileList &lst, off_t offset = 0, struct fuse_file_info *fi = NULL);
  int mkdir(const std::string &path, mode_t mode);
  int rmdir(const std::string &path);
  int mknod(const std::string &path, mode_t mode, dev_t dev);
  int chmod(const std::string &path, mode_t mode);
};

class TContent
{
 public:
  virtual ~TContent(){};

  virtual int open(const std::string &path, fuse_file_info *) = 0;
  virtual int release(const std::string &path, struct fuse_file_info *fi) = 0;
  virtual int read(const std::string &path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi) = 0;
  virtual int write(const std::string &path, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi) = 0;
  virtual int truncate(const std::string &full_name, off_t size) = 0;
  virtual int unlink(const std::string &full_name) = 0;
};

class TfsContent : public TContent
{
 public:

  int open(const std::string &full_name, struct fuse_file_info *fi);
  int release(const std::string &full_name, struct fuse_file_info *fi);
  int read(const std::string &full_name, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi);
  int write(const std::string &full_name, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi);
  int truncate(const std::string &full_name, off_t size);
  int unlink(const std::string &full_name);
};

class TMetaContent
{
 public:
  std::string root;
  TMeta * meta;
  TContent * content;
};

class TSQLfs
{
 public:
  TSQLfs();
  virtual ~TSQLfs(){};

  void add(const std::string &root, TMeta *, TContent *);
  int start(const std::string argv0, const std::string mount_point);

  virtual int fgetattr(const char *path, struct stat *stbuf);
  virtual int freaddir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
  virtual int fmkdir(const char *path, mode_t mode);
  virtual int funlink(const char *path);
  virtual int frmdir(const char *path);
  virtual int fmknod(const char *path, mode_t mode, dev_t dev);
  virtual int fchmod(const char *path, mode_t mode);
  virtual int ftruncate(const char *, off_t);
  virtual int fopen(const char *path, fuse_file_info * fi);
  virtual int frelease(const char *path, struct fuse_file_info *fi);
  virtual int fread(const char *path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi);
  virtual int fwrite(const char *path, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi);

 protected:
  std::list<TMetaContent *> lst_fs;
  struct fuse_operations fuse_op;
};

//---------------------------------------------------------------------------
class TMutexLock
{
 public:
  TMutexLock(pthread_mutex_t * pmutex, bool plock = true) : locked(false)
  {
   mutex = pmutex;
   if (plock) lock();
  };
  ~TMutexLock()
  {
   if (locked) unlock();
  }
  void lock(){pthread_mutex_lock(mutex); locked = true;};
  void unlock(){pthread_mutex_unlock(mutex); locked = false;};

 private:
  pthread_mutex_t * mutex;
  bool locked;
};
//---------------------------------------------------------------------------
const int split_str(std::vector<std::string> & vct, const std::string &str, const std::string &separator = "/");
const int parse_path(const std::string &full_path, std::string &path, std::string & name);
#endif // SQLFS_H
