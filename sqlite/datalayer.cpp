#include <sstream>
#include <fstream>
#include <string>
#include <cstring>

#ifdef TEST_BUILD
#include <gtest/gtest.h>
#endif

#include "datalayer.h"

//--------------------------------------------------------------------------
TSQLayer::TSQLayer()
{
 db = NULL;
 zErrMsg = 0;
 nameid.clear();
 name_length = 0;
 trN = 0;
 pthread_mutex_init(&mtx, NULL);
}
TSQLayer::~TSQLayer()
{
// stop();
 close();
}
//--------------------------------------------------------------------------
int TSQLayer::open()
{
 int ret(0);

#ifdef _MSC_VER
 char tmppath[MAX_PATH];
 memset(tmppath, 0, sizeof(tmppath));
 if (GetTempPathA(MAX_PATH, tmppath) > 0)
 {
  sqlite3_temp_directory = sqlite3_mprintf("%s", tmppath);
 }
 else
 {
  ShowMessage(0, Wt::WString::fromUTF8("Ошибка получения пути к папке временных файлов.\nОбратитесь с системному администратору."));
 }
#else
 sqlite3_temp_directory = sqlite3_mprintf("/tmp");
#endif

 TMutexLock lock(&mtx);
 ret = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE, NULL);

 if (ret == SQLITE_CANTOPEN)
 {
  ret = sqlite3_open_v2(dbname.c_str(), &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);
  if (ret == SQLITE_OK) ret = create_struct();
 }

// if (ret == SQLITE_OK) ret = check_version();

 return(ret);
}
bool TSQLayer::isOpen()
{
 TMutexLock lock(&mtx);
 return (db != NULL);
}
//--------------------------------------------------------------------------
void TSQLayer::close()
{
 TMutexLock lock(&mtx);
 sqlite3_close(db);
}
//--------------------------------------------------------------------------
/*int TSQLayer::setQuery(const std::string& sql)
{
 return(setQuery(sql, st));
}*/
int TSQLayer::setQuery(const std::string& sql/*, sqlite3_stmt * st*/)
{
 int ret;

 ret = sqlite3_prepare_v2(db, sql.c_str(), sql.length(), &st, NULL);

 return (ret);
}
//--------------------------------------------------------------------------
int TSQLayer::setParam(const std::string& name, const std::string& v)
{
 int ret;
 int parameter_idx;
 std::string param(std::string(":") + name);

 parameter_idx = sqlite3_bind_parameter_index(st, param.c_str());
 if (parameter_idx == 0)
 {
  return(-1);
 }
 ret = sqlite3_bind_text(st, parameter_idx, v.c_str(), v.length(), SQLITE_TRANSIENT);
 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::setParam(const std::string& name, const int v)
{
 int ret;
 int parameter_idx;
 std::string param(std::string(":") + name);
 parameter_idx = sqlite3_bind_parameter_index(st, param.c_str());
 if (parameter_idx == 0)
 {
  return(-1);
 }
 ret = sqlite3_bind_int(st, parameter_idx, v);
 return(ret);
}
int TSQLayer::setParam(const std::string& name, const int64_t v)
{
 int ret;
 int parameter_idx;
 std::string param(std::string(":") + name);
 parameter_idx = sqlite3_bind_parameter_index(st, param.c_str());
 if (parameter_idx == 0)
 {
  return(-1);
 }
 ret = sqlite3_bind_int64(st, parameter_idx, v);
 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::setParam(const std::string& name, const void* buff, const int length)
{
 int ret;
 int parameter_idx;
 std::string param(std::string(":") + name);

 parameter_idx = sqlite3_bind_parameter_index(st, param.c_str());
 if (parameter_idx == 0)
 {
  return(-1);
 }
 ret = sqlite3_bind_blob(st, parameter_idx, buff, length, SQLITE_TRANSIENT);
 return(ret);
}
//--------------------------------------------------------------------------
int TSQLayer::setParamNull(const std::string& name)
{
 int ret;
 int parameter_idx;
 std::string param(std::string(":") + name);
 parameter_idx = sqlite3_bind_parameter_index(st, param.c_str());
 if (parameter_idx == 0)
 {
  return(-1);
 }
 ret = sqlite3_bind_null(st, parameter_idx);
 return(ret);
}
//--------------------------------------------------------------------------
/*int TSQLayer::exec(Wt::WAbstractItemModel * mt)
{
 int ret(0);

 while(sqlite3_step(st) == SQLITE_ROW)
 {
  st_to_mt(mt, st);
  ret++;
 }

 sqlite3_finalize(st);

 return(ret);
}*/
//--------------------------------------------------------------------------
/*bool TSQLayer::exec()
{
 return (exec(st));
}*/

bool TSQLayer::exec(/*sqlite3_stmt * stm*/)
{
 bool ret(false);

 if(sqlite3_step(st) == SQLITE_DONE)
 {
  ret = true;
 }

 sqlite3_finalize(st);
 return(ret);
}

int TSQLayer::begin(void)
{
 if (trN == 0)
 {
  std::string sql("BEGIN TRANSACTION;");
  int ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
  if (ret != SQLITE_OK)
  {
    return(-1);
  }
 }
 trN++;
 return(trN);
}
int TSQLayer::commit(void)
{
 trN--;
 if (trN == 0)
 {
   std::string sql("COMMIT;");
   int ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
   if (ret != SQLITE_OK)
   {
     return(-1);
   }
 }
 if (trN < 0)
 {
  trN = 0;
 }
 return (trN);
}
void TSQLayer::rollback(void)
{
 std::string sql("ROLLBACK;");
 int ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
 if (ret != SQLITE_OK)
 {
   return;
 }
 trN = 0;
}
//--------------------------------------------------------------------------
std::string TSQLayer::errmsg()
{
 return(sqlite3_errmsg(db));
}
//--------------------------------------------------------------------------
int TSQLayer::create_struct()
{
// extern TParameterList params;
 int ret(-1);
 std::string sql;
 std::string sqlfile; // docroot
// if (Wt::WApplication::instance() != NULL) sqlfile = (Wt::WApplication::instance()->docRoot());
// else sqlfile = params.get("docroot")->v_str + "/";

 sqlfile += "sqlite/sql/struct.sql";

 sql = read_file(sqlfile);

 if (!sql.empty())
 {
   ret = sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
 }

 if (ret == SQLITE_OK)
 {
  sql = read_file("sqlite/sql/data.sql");
  if (!sql.empty())
  {
   sqlite3_exec(db, sql.c_str(), NULL, NULL, NULL);
  }

 }

 return(ret);
}
//--------------------------------------------------------------------------
std::string TSQLayer::read_file(const std::string& filename)
{
 std::stringstream stm;
 std::ifstream src;
 char buff[4096];

 src.open(filename.c_str());
 if (!src.is_open()) {return (std::string()); }

// src.seekg (0, src.beg);
 while(!src.eof())
 {
  memset(buff, 0, 4096);
  src.read(buff, 4096);           // чтение блока
  if(src.gcount() > 0) stm << buff;
 }

 src.close();

 return(stm.str());
}
//--------------------------------------------------------------------------
int TSQLayer::file_s(TFileList &lst,const int file_id, const int parent_id)
{
 int ret;
 std::string sql;

 sql = "select file_id, parent, name, mode, strftime('%s', atime), strftime('%s', mtime), ";
 sql+= "strftime('%s', ctime), uid, gid, size ";
 sql+= "from file ";
 if (file_id > 0)
 {
  sql += "where file_id=:file_id ";
 }
 else if (parent_id > 0)
  sql+= "where parent = :parent ";
 else
  sql+= "where parent is NULL ";
 sql+= "order by name ";

 lst.clear();

 ret = setQuery(sql);
 if (ret != SQLITE_OK) return (-1);
 if (file_id > 0)
  ret = setParam("file_id", file_id);
 if (ret != SQLITE_OK) return (-1);
 if (parent_id > 0)
  ret = setParam("parent", parent_id);
 if (ret != SQLITE_OK) return (-1);

 if (ret == SQLITE_OK)
 {
  for(ret = 0; sqlite3_step(st) == SQLITE_ROW; ret++)
  {
   TFileAttr * attr  = new TFileAttr();
   attr->st.st_dev   = 25;
   attr->st.st_ino   = sqlite3_column_int(st, 0);
   attr->parent      = sqlite3_column_int(st, 1);
   attr->file_name   = std::string((const char *)sqlite3_column_text(st, 2), sqlite3_column_bytes(st, 2));
   attr->st.st_mode  = sqlite3_column_int(st, 3);
   attr->st.st_atime = sqlite3_column_int(st, 4);
   attr->st.st_mtim.tv_sec = sqlite3_column_int(st, 5);
//   std::cout << "TSQLayer::file_s attr.st.st_mtim.tv_sec=" << sqlite3_column_int(st, 5) << std::endl;
   attr->st.st_ctime = sqlite3_column_int(st, 6);
   attr->st.st_uid   = sqlite3_column_int(st, 7);
   attr->st.st_gid   = sqlite3_column_int(st, 8);
   attr->st.st_size  = sqlite3_column_int64(st, 9);
   lst.push_back(attr);
  }

  sqlite3_finalize(st);
 }

 return(ret);
}
int TSQLayer::file_iu(const int parent_id, TFileAttr &attr)
{
 int ret;
 std::string sql;
 int id;

 sql = "insert or replace into file(file_id, parent, name, mode, atime, mtime, ctime, ";
 sql+= "uid, gid, size) ";
 sql+= "values(:file_id, :parent, :name, :mode, :atime, :mtime, :ctime, ";
 sql+= ":uid, :gid, :size) ";
 sql+= "ON CONFLICT(file_id) DO UPDATE SET ";
 sql+= "parent=excluded.parent, name=excluded.name, ";
 sql+= "mode=excluded.mode, atime=excluded.atime, mtime=excluded.mtime, ";
 sql+= "ctime=excluded.ctime, uid=excluded.uid, gid=excluded.gid, size=excluded.size; ";

 ret = setQuery(sql);
 if (ret != SQLITE_OK) return (-1);
 id = attr.st.st_ino > 0 ? attr.st.st_ino : getId("file");
 setParam("file_id", id);
 if (parent_id > 0) setParam("parent", parent_id);
 setParam("name", attr.file_name);
 setParam("mode", (int)attr.st.st_mode);
 setParam("atime", timet_to_a(&attr.st.st_atime));
 setParam("mtime", timet_to_a(&attr.st.st_mtime));
 setParam("ctime", timet_to_a(&attr.st.st_ctime));
 setParam("uid",  (int)attr.st.st_uid);
 setParam("gid",  (int)attr.st.st_gid);
 setParam("size", (int64_t)attr.st.st_size);

 return(exec() ? id: -1);
}

int TSQLayer::file_d(const int file_id)
{
 int ret;
 std::string sql;

 sql = "delete from file ";
 sql+= "where file_id = :file_id; ";

 ret = setQuery(sql);
 if (ret != SQLITE_OK) return (-1);
 setParam("file_id", file_id);

 return(exec() ? 0 : -1);
}

int TSQLayer::node_s(TNodeList & lst, const int file_id, const int64_t off, const int size)
{
 int ret;
 std::string sql;

 sql = "select node_id, file_id, off, size, content ";
 sql +="from node ";
 sql +="where file_id=:file_id ";
 sql +="order by off ASC ";
 ret = setQuery(sql);
 if (ret != SQLITE_OK) return (ret);
 setParam("file_id", file_id);
 if (ret == SQLITE_OK)
 {
  for(ret = 0; sqlite3_step(st) == SQLITE_ROW; ret++)
  {
   int blob_size = sqlite3_column_bytes(st, 4);
   TNode * node = new TNode(blob_size);
   node->node_id = sqlite3_column_int(st, 0);
   node->off     = sqlite3_column_int(st, 2);
   //node->set_size(sqlite3_column_int(st, 3));
   const void * blob = sqlite3_column_blob(st, 4);
   if (blob_size > 0 && blob != NULL)
    node->write(blob, blob_size);
   node->usize = blob_size;
   lst.push_back(node);
  }
 }
 sqlite3_finalize(st);

 return(ret);
}

int TSQLayer::node_iu(TNodeList & lst, const int file_id)
{
 for(TNodeList::iterator itr = lst.begin(); itr != lst.end(); ++itr)
 {
  if (!(*itr)->modified /*&& (*itr)->node_id > 0*/) continue;

  int id = node_iu(*itr, file_id);
  if (id > 0)
    (*itr)->node_id = id;
  else
     return(-1);
 }
 return(1);
}

int TSQLayer::node_iu(TNode * node, const int file_id)
{
 int ret;
 std::string sql;

 sql = "insert or replace into node(node_id, file_id, off, size, content) ";
 sql +="values(:node_id, :file_id, :off, :size, :content) ";
 sql+= "ON CONFLICT(node_id) DO UPDATE SET ";
 sql+= "file_id=excluded.file_id, ";
 sql+= "off=excluded.off, size=excluded.size, content=excluded.content; ";

//  if (!(*itr)->modified) continue;
  ret = setQuery(sql);
  if (ret != SQLITE_OK) return (-1);
  int id = node->node_id > 0 ? node->node_id : getId("node");
  setParam("node_id", id);
  setParam("file_id", file_id);
  setParam("off", node->off);
  setParam("size", node->usize);
  if (node->usize > 0)
    setParam("content", node->operator[](0), node->usize);
  else
    setParamNull("content");
  if (!exec()) return(-1);

 return(id);
}

int TSQLayer::node_d(const int file_id, const int node_id)
{
 int ret;
 std::string sql;

 sql = "delete from node ";
 sql+= "where ";
 if (node_id > 0)
   sql+= "node_id = :id; ";
 else
   sql+= "file_id = :id; ";

 ret = setQuery(sql);
 if (ret != SQLITE_OK) return (-1);
 setParam("id", node_id > 0 ? node_id : file_id);

 return(exec() ? 0 : -1);
}

const int TSQLayer::getId(const std::string& pname)
{
 sqlite3_stmt * stm;
 std::string name(/*Upper*/(pname));

 if (!isOpen()){return (-1);}

 TMutexLock lock(&mtx);

 if (nameid.find(name) == nameid.end())
 {
  nameid[name] = 0;
  std::string sql;
  sql = "select max(" + name +"_id) from " + name;
  //setQuery(sql, stm);
  sqlite3_prepare_v2(db, sql.c_str(), sql.length(), &stm, NULL);

  if(sqlite3_step(stm) == SQLITE_ROW)
  {
   nameid[name] = sqlite3_column_int(stm, 0);
  }
  sqlite3_finalize(stm);
 }
 return(++nameid[name]);
}

const int TSQLayer::path2id(const std::string &path)
{
 TFileAttr attr;
 return(path2id(path, attr));
}

const int TSQLayer::path2id(const std::string &path, TFileAttr &attr)
{
 int id(0);
 std::vector<std::string> vct;
 TFileList lst;

 if (path == "/") return (0);

 split_str(vct, path);

 for(unsigned int i = 0; i < vct.size(); i++)
 {
  file_s(lst, 0, id);
  id = 0;
  for(TFileList::iterator itr = lst.begin(); itr != lst.end(); ++itr)
  {
   if(vct[i] == (*itr)->file_name)
   {
    id = (*itr)->st.st_ino;
    attr.clear();
    attr.file_name = (*itr)->file_name;
    attr.parent    = (*itr)->parent;
    (*itr)->copyTo(&attr.st);
    break;
   }
  }
  if(id == 0) return -1;
 }

 return (id);
}

const std::string TSQLayer::timet_to_a(const time_t * tt)
{
 char s[128]; struct tm loc_time; struct tm * pt = &loc_time;

 pt = gmtime(tt);
 sprintf(s,"%04d-%02d-%02d %02d:%02d:%02d",
           pt->tm_year + 1900, pt->tm_mon + 1, pt->tm_mday,
           pt->tm_hour, pt->tm_min, pt->tm_sec);

 return(std::string(s));
}
//--------------------------------------------------------------------------

#ifdef TEST_BUILD
class TestSqlite : public ::testing::Test
{
 public:
  TestSqlite();// { /* init protected members here */ }
 ~TestSqlite();// { /* free protected members here */ }
// void SetUp();// { /* called before every test */ }
// void TearDown();// { /* called after every test */ }

 protected:
  TSQLayer * sl;
};

TestSqlite::TestSqlite()
{ /* init protected members here */
 std::remove("test_run/testdb.db");

 sl = new TSQLayer();
 sl->dbname = "test_run/testdb.db";
 sl->open();
}
TestSqlite::~TestSqlite()
{ /* free protected members here */
 delete(sl);
 std::remove("test_run/testdb.db");
}

TEST_F(TestSqlite, TSQLayerIsOpened)
{
 ASSERT_TRUE(sl->isOpen());
}

TEST_F(TestSqlite, FileS)
{
 TFileList lst;
 ASSERT_TRUE(sl->isOpen());
 int ret = sl->file_s(lst, 0, 0);
 ASSERT_TRUE(ret >= 0);
 ASSERT_TRUE(lst.size() >= 0);

 lst.clear();
 ret = sl->file_s(lst, 1);
 ASSERT_EQ(1, ret) << "error sl->file_s" << std::endl << sl->errmsg() << std::endl;
 ASSERT_EQ(1, lst.size());

 lst.clear();
 ret = sl->file_s(lst, 0, 1);
 ASSERT_EQ(1, ret) << "error sl->file_s" << std::endl << sl->errmsg() << std::endl;
 ASSERT_EQ(1, lst.size());
}

TEST_F(TestSqlite, GetId)
{
 int ret = sl->getId("file");

 ASSERT_TRUE(ret > 0);
 ASSERT_EQ(ret + 1, sl->getId("file"));

 ret = sl->getId("node");

 ASSERT_TRUE(ret > 0);
 ASSERT_EQ(ret + 1, sl->getId("node"));
}

TEST_F(TestSqlite, Path2Id)
{
 TFileAttr attr;

 int ret = sl->path2id("/empty/dir");
 ASSERT_EQ(-1, ret);

 ret = sl->path2id("/hello_world");
 ASSERT_EQ(1, ret);
 ret = sl->path2id("/hello_world/test");
 ASSERT_EQ(2, ret);
}

TEST_F(TestSqlite, FileIU_DIR)
{
 TFileAttr attr;
 attr.file_name = "test2";
 attr.st.st_mode = S_IFDIR | 0777;
 int ret = sl->file_iu(1, attr);

 ASSERT_TRUE(ret > 0) << sl->errmsg();

 ret = sl->getId("/hello_world/test2");
 ASSERT_TRUE(ret > 0);
}

TEST_F(TestSqlite, FileIU_FILE)
{
 TFileList lst;
 TFileAttr attr;
 time_t curent_timestamp = time(NULL);
 attr.file_name = "file.test";
 attr.st.st_mode = S_IFREG | 0644;
 attr.st.st_size = 512;
// attr.st.st_atim.tv_sec = curent_timestamp;
 attr.st.st_mtime = curent_timestamp;
// attr.st.st_ctime = curent_timestamp;

 int id = sl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << sl->errmsg();

 sl->file_s(lst, id);

 ASSERT_EQ(1, lst.size());
 TFileList::iterator itr = lst.begin();
 ASSERT_EQ(512, (*itr)->st.st_size);
 ASSERT_EQ(curent_timestamp, (*itr)->st.st_mtime) << "timestamp falls " <<  std::endl;

 id = sl->getId("/hello_world/file.test");
 ASSERT_TRUE(id > 0);

 attr.st.st_size = 2048;
 id = sl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << sl->errmsg();

 sl->file_s(lst, id);

 ASSERT_EQ(1, lst.size());
 itr = lst.begin();
 ASSERT_EQ(2048, (*itr)->st.st_size);
}

TEST_F(TestSqlite, FileD)
{
 TFileList lst;
 TFileAttr attr;
 time_t curent_timestamp = time(NULL);
 attr.file_name = "file.test";
 attr.st.st_mode = S_IFREG | 0644;
 attr.st.st_size = 512;
 attr.st.st_mtime = curent_timestamp;

 int id = sl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << sl->errmsg();

 sl->file_s(lst, id);

 ASSERT_EQ(1, lst.size());

 int ret = sl->file_d(id);

 ASSERT_EQ(0, ret);

 sl->file_s(lst, id);

 ASSERT_EQ(0, lst.size());
}

TEST_F(TestSqlite, NodeIU)
{
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = sl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << sl->errmsg();

 TNodeList lst;

 for(int i = 0; i < 65536; i += 1024)
 {
  TNode * node = new TNode(1024);
  node->off = i;
  node->modified = true;
  node->usize = node->get_size();
  lst.push_back(node);
 }

 int ret = sl->node_iu(lst, id);
 ASSERT_EQ(1, ret) << "node_iu are falls" << std::endl << sl->errmsg();
}

TEST_F(TestSqlite, NodeD)
{
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = sl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << sl->errmsg();

 int ret = sl->path2id("/hello_world/nodeiu");
 ASSERT_TRUE(ret > 0);

 TNodeList lst;

 const int NodeSize = sizeof(int) * 1024;

 for(int i = 0; i < 10; i++)
 {
  TNode * node = new TNode(NodeSize);
  node->off = i * NodeSize;
  node->modified = true;
  node->usize = NodeSize;
  lst.push_back(node);
 }

 ret = sl->node_iu(lst, id);
 ASSERT_EQ(1, ret) << "node_iu are falls" << std::endl
                   << sl->errmsg() << std::endl;

 TNodeList lst2;
 ret = sl->node_s(lst2, id, 0, 0);
 ASSERT_EQ(10, ret) << "node_s count=" << ret << std::endl;
 ASSERT_EQ(lst.size(), lst2.size()) << "lst.size() != lst2.size()" << std::endl;

 ret = sl->node_d(0, lst2.back()->node_id);
 ASSERT_EQ(0, ret) << "sl->node_d(id) false" << std::endl;

 TNodeList lst3;
 ret = sl->node_s(lst3, id, 0, 0);
 ASSERT_EQ(9, ret) << "node_s count=" << ret << std::endl;
 ASSERT_EQ(9, lst3.size()) << "lst3.size() = " << lst3.size() << std::endl;

 ret = sl->node_d(id);
 ASSERT_EQ(0, ret) << "sl->node_d(id) false" << std::endl;

 TNodeList lst4;
 ret = sl->node_s(lst4, id, 0, 0);
 ASSERT_EQ(0, ret) << "node_s count=" << ret << std::endl;
 ASSERT_EQ(0, lst4.size()) << "lst4.size() = " << lst4.size() << std::endl;
}

TEST_F(TestSqlite, NodeS)
{
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = sl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << sl->errmsg();

 int ret = sl->path2id("/hello_world/nodeiu");
 ASSERT_TRUE(ret > 0);

 TNodeList lst;

 const int NodeSize = sizeof(int) * 1024;

 for(int i = 0; i < NodeSize * 10; i += NodeSize)
 {
  TNode * node = new TNode(NodeSize);
  node->off = i;
  node->modified = true;

  node->set_position(0);
  for(unsigned int x = 0; x + sizeof(x) <= NodeSize; x += sizeof(x))
  {
   node->write(&x, sizeof(x));
   node->usize += sizeof(x);
  }

  lst.push_back(node);
 }

 ret = sl->node_iu(lst, id);
 ASSERT_EQ(1, ret) << "node_iu are falls" << std::endl
                   << sl->errmsg() << std::endl;

 TNodeList lst2;
 ret = sl->node_s(lst2, id, 0, 0);
 ASSERT_TRUE(ret > 0) << "node_s count=" << ret;

 ASSERT_EQ(lst.size(), lst2.size()) << "lst.size() != lst2.size()";

 int i = 0;
 for(TNodeList::iterator itr = lst2.begin(); itr != lst2.end(); ++itr, i += NodeSize)
 {
  ASSERT_EQ(NodeSize, (*itr)->get_size());
  ASSERT_EQ(NodeSize, (*itr)->usize);
  ASSERT_EQ(i, (*itr)->off);
  ASSERT_FALSE((*itr)->modified);

  (*itr)->set_position(0);
  for(unsigned int x = 0; x + sizeof(x) < NodeSize; x += sizeof(x))
  {
   int v(0);
   (*itr)->read(&v, sizeof(v));
   ASSERT_EQ(x, v);
  }
 }

}
#endif
