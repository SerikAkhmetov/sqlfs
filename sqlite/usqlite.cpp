#include <map>

#include <time.h>

#include "datalayer.h"
#include "openedfile.h"
#include "usqlite.h"

#ifdef TEST_BUILD
#include <cstring>
#include <gtest/gtest.h>
#endif

//-----------------------------------------------------------------------
class TsqliteMeta : public TMeta
{
 public:
  TsqliteMeta(const std::string &dbfile);
  virtual ~TsqliteMeta();

  int getattr(const std::string &path, TFileAttr & attr);
//  virtual int access(const std::string &path, int mask);
  int readdir(const std::string &path, TFileList &lst, off_t offset = 0, struct fuse_file_info *fi = NULL);
  int mkdir(const std::string &path, mode_t mode);
  int rmdir(const std::string &path);
  int mknod(const std::string &path, mode_t mode, dev_t dev);
  int chmod(const std::string &path, mode_t mode);
// protected:
  TSQLayer * dl;
};
//-----------------------------------------------------------------------

class TsqliteContent : public TContent
{
 public:

  int open(const std::string &path, struct fuse_file_info * fi);
  int release(const std::string &path, struct fuse_file_info *fi);
  int read(const std::string &path, char *buf, size_t size, off_t offset,
                      struct fuse_file_info *fi);
  int write(const std::string &path, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi);
  int truncate(const std::string &full_name, off_t size);
  int unlink(const std::string &full_name);
  // protected:
  TSQLayer * dl;

// protected:
  std::map<std::string, TOpenedFile *> openedFile;
};
//-----------------------------------------------------------------------

TsqliteMeta::TsqliteMeta(const std::string &dbfile)
{
 dl = new TSQLayer();
 dl->dbname = dbfile;
 dl->open();
}

TsqliteMeta::~TsqliteMeta()
{
 delete(dl);
}

int TsqliteMeta::getattr(const std::string &path, TFileAttr & attr)
{
 TFileList lst;
 std::string dir_name, file_name;

 if (parse_path(path, dir_name, file_name) != 1)
 {
   return(-ENOENT);
 }
 int ret = readdir(dir_name, lst);
 if (ret == 0)
 {
  for(TFileList::iterator itr = lst.begin(); itr != lst.end(); ++itr)
  {
   if ((*itr)->file_name == file_name)
   {
    (*itr)->copyTo(&attr.st);

    attr.file_name = (*itr)->file_name;
    return(0);
   }
  }
  return (-ENOENT);
 }
 return(ret);
}


int TsqliteMeta::readdir(const std::string &path, TFileList &lst, off_t offset, struct fuse_file_info *fi)
{
 int id(dl->path2id(path));

 if (id < 0) return -ENOENT;
 dl->file_s(lst, 0, id);
 return(0);
}

int TsqliteMeta::mkdir(const std::string &path, mode_t mode)
{
 int ret;
 std::string dir_name, file_name;

 if (parse_path(path, dir_name, file_name) != 1)
   return(-ENOENT);
 int id(dl->path2id(dir_name));
 if (id < 0) return -ENOENT;
 TFileAttr attr;
 attr.file_name = file_name;
 attr.st.st_mode = mode | S_IFDIR;
#ifndef TEST_BUILD // crash in test mode
 fuse_context *fc;
 attr.st.st_uid = (fc = fuse_get_context()) != NULL ? fc->uid : 0;
 attr.st.st_gid = (fc = fuse_get_context()) != NULL ? fc->gid : 0;
#endif
 if(id >= 0) ret = dl->file_iu(id, attr);

 return(ret > 0 ? 0 : -ENOENT);
}

int TsqliteMeta::rmdir(const std::string &path)
{
 int id; TFileAttr attr;

 id = dl->path2id(path, attr);
 if (id > 0)
 {
  return(dl->file_d(id));
 }
 else
 {
  return(-ENOENT);
 }
}

int TsqliteMeta::mknod(const std::string &path, mode_t mode, dev_t dev)
{
 (void) dev;
 std::string dir_name, file_name;

 if (dl->path2id(path) > 0) return (0);

 if (parse_path(path, dir_name, file_name) != 1)
   return(-ENOENT);
 int id(dl->path2id(dir_name));
 if (id < 0) return -ENOENT;

 TFileAttr attr;
 attr.file_name = file_name;
 attr.st.st_mode = mode | S_IFREG;
 attr.st.st_ctime = time(NULL);
 attr.st.st_atime = time(NULL);
 attr.st.st_mtime = time(NULL);
#ifndef TEST_BUILD // crash in test mode
 fuse_context *fc;
 attr.st.st_uid = (fc = fuse_get_context()) != NULL ? fc->uid : 0;
 attr.st.st_gid = (fc = fuse_get_context()) != NULL ? fc->gid : 0;
#endif
 if(id >= 0) dl->file_iu(id, attr);
  else return -ENOENT;

 return(0);
}

int TsqliteMeta::chmod(const std::string &path, mode_t mode)
{
 TFileList lst;

 int id(dl->path2id(path));
 if (id < 0){return -ENOENT;}

 if (dl->file_s(lst, id) == 1)
 {
  if(lst.front()->st.st_mode & S_IFREG)
    lst.front()->st.st_mode = mode | S_IFREG;
  else if (lst.front()->st.st_mode & S_IFDIR)
    lst.front()->st.st_mode = mode | S_IFDIR;
  else {return -ENOENT;}

  dl->file_iu(lst.front()->parent, *lst.front());
  return(0);
 }

 return -ENOENT;
}

//-----------------------------------------------------------------------

int TsqliteContent::open(const std::string &full_name, struct fuse_file_info * fi)
{
 if (fi->flags & O_EXCL && fi->flags & O_CREAT) { return(-ENOENT); }

 std::map<std::string, TOpenedFile *>::iterator itr = openedFile.find(full_name);
 if (itr == openedFile.end())
 {
  int id;
  TOpenedFile * op; TFileAttr attr;
  id = dl->path2id(full_name, attr);
  if (id < 0)
  {return -ENOENT;}
// TODO : check file mode
  op = new TOpenedFile(dl);
  op->attr.file_name = attr.file_name;
  op->attr.parent = attr.parent;
  attr.copyTo(&op->attr.st);
  op->mode    = fi->flags;
  op->init();
  openedFile.insert(std::pair<std::string, TOpenedFile *>(full_name, op));
  fi->fh = id;
  return(0);
 }
 return(0);
}

int TsqliteContent::release(const std::string &full_name, struct fuse_file_info *fi)
{
 int ret(0);
 std::map<std::string, TOpenedFile *>::iterator itr = openedFile.find(full_name);
 if (itr != openedFile.end())
 {
  ret = itr->second->save() ? 0 : -1;
  delete(itr->second);
  openedFile.erase(itr);
 }
 return(ret);
}

int TsqliteContent::read(const std::string &full_name, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi)
{
 std::map<std::string, TOpenedFile *>::iterator itr = openedFile.find(full_name);
 if (itr == openedFile.end())
 {
  return -ENOENT;
 }
 return(itr->second->read(buf, size, offset));
}

int TsqliteContent::write(const std::string &full_name, const char *buf, size_t size,
                   off_t offset, struct fuse_file_info *fi)
{
 std::map<std::string, TOpenedFile *>::iterator itr = openedFile.find(full_name);
 if (itr == openedFile.end())
 {
  return -ENOENT;
 }
 return(itr->second->write(buf, size, offset));
}

int TsqliteContent::truncate(const std::string &full_name, off_t size)
{
 TFileAttr attr;
 int id(0);
 std::map<std::string, TOpenedFile *>::iterator itr = openedFile.find(full_name);
 if (itr != openedFile.end())
 {
  attr.parent = itr->second->attr.parent;
  attr.file_name = itr->second->attr.file_name;
  itr->second->attr.copyTo(&attr.st);
  id = itr->second->attr.st.st_ino;
  delete(itr->second);
  openedFile.erase(itr);
 }

 if (id == 0)
   id = dl->path2id(full_name, attr);
 if (id < 0)
 {return -ENOENT;}

 if (size == attr.st.st_size)
 {
  return(0);
 }
 else if (size == 0)
 {
  dl->node_d(id);
 }
 else if (size > attr.st.st_size)
 {
  TNodeList lst;
  dl->node_s(lst, id, 0, 0);
  while(size < attr.st.st_size)
  {
   TNode * last;
   if (lst.empty())
   {
    last = new TNode(TOpenedFile::NodeSize);
    lst.push_back(last);
   }
   if (lst.back()->get_size() < TOpenedFile::NodeSize)
   {
    last = lst.back();
    last->set_size(TOpenedFile::NodeSize);
   }
   else if (lst.back()->usize == lst.back()->get_size())
   {
    last = new TNode(TOpenedFile::NodeSize);
    last->off = lst.back()->off + lst.back()->usize;
    lst.push_back(last);
   }

   if (size >= last->off + last->get_size())
   {last->usize = last->get_size();}
   else
   {last->usize += size - (last->off + last->usize);}

   attr.st.st_size = last->off + last->usize;
   last->modified = true;
   dl->node_iu(last, id);
  }
 }
 else // size < attr.st.st_size
 {
  TNodeList lst;
  dl->node_s(lst, id, 0, 0);
  for(TNodeList::iterator itr = lst.begin(); itr != lst.end(); ++itr)
  {
   if ((*itr)->off + (*itr)->get_size() <= size) continue;
   if ((*itr)->off >= size)
   {dl->node_d(0, (*itr)->node_id);}
   if ((*itr)->off <= size && (*itr)->off + (*itr)->get_size() > size)
   {
    (*itr)->set_size(size - (*itr)->off);
    (*itr)->usize = (*itr)->get_size();
    (*itr)->modified = true;
    dl->node_iu((*itr), id);
   }
  }
 }
 attr.st.st_mtime = time(NULL);
 attr.st.st_size = size;
 dl->file_iu(attr.parent, attr);
 return(0);
}

int TsqliteContent::unlink(const std::string &full_name)
{
 TFileAttr attr;
 int id(0);

 if (openedFile.find(full_name) != openedFile.end())
 {
  return(-EACCES);
 }

 id = dl->path2id(full_name, attr);
 if (id < 0)
 {
  return(-ENOENT);
 }

 if (dl->node_d(id) == 0 && dl->file_d(id) == 0)
 {
  return(0);
 }
 else
 {
  return(-ENOENT);
 }
}
//-----------------------------------------------------------------------

TMeta * MetaSQLiteFactory(const std::string &dbfile)
{
 TsqliteMeta * meta = new TsqliteMeta(dbfile);
 if (!meta->dl->isOpen())
 {
  delete(meta);
  meta = NULL;
 }
 return (meta);
}
TContent * ContentSQLiteFactory(TMeta * m)
{
 TsqliteContent * content = NULL;
 TsqliteMeta * meta = dynamic_cast<TsqliteMeta *>(m);
 if (meta != NULL)
 {
  content = new TsqliteContent();
  content->dl = meta->dl;
 }
 return (content);
}
//-----------------------------------------------------------------------

#ifdef TEST_BUILD

class TestMetaSqlite : public ::testing::Test
{
 public:
  TestMetaSqlite();// { /* init protected members here */ }
 ~TestMetaSqlite();// { /* free protected members here */ }
 void SetUp();// { /* called before every test */ }
 void TearDown();// { /* called after every test */ }

 protected:
  TsqliteMeta * meta;
};

TestMetaSqlite::TestMetaSqlite()
{ /* init protected members here */
 std::remove("test_run/testdb.db");

// meta = MetaSQLiteFactory("test_run/testdb.db");
  meta = new TsqliteMeta("test_run/testdb.db");
}
TestMetaSqlite::~TestMetaSqlite()
{ /* free protected members here */
 delete(meta);
 std::remove("test_run/testdb.db");
}
void TestMetaSqlite::SetUp()
{ /* called before every test */
}
void TestMetaSqlite::TearDown()
{ /* called after every test */
}

TEST_F(TestMetaSqlite, ReadDir)
{
 TFileList lst;

 int ret = meta->readdir("/", lst);
 ASSERT_EQ(0, ret);
 ASSERT_TRUE(lst.size() >= 0);
//std::cout << "lst.size = " << lst.size() << std::endl;
}

TEST_F(TestMetaSqlite, GetAttr)
{
 TFileAttr attr;

 int ret; // = meta->getattr("/", attr);
// ASSERT_EQ(0, ret) << "Falls for /";

 ret = meta->getattr("/empty/dir", attr);
 ASSERT_TRUE(ret < 0) << "Falls for /empty/dir" << std::endl;

 ret = meta->getattr("/hello_world", attr);
 ASSERT_EQ(0, ret) << "Falls for /hello_world" << std::endl;
 ASSERT_EQ("hello_world", attr.file_name) << "Falls for attr.file_name" << std::endl;
 ASSERT_EQ(S_IFDIR, attr.st.st_mode & S_IFDIR) << "Falls for S_IFDIR" << std::endl;

 ret = meta->getattr("/hello_world/test", attr);
 ASSERT_EQ(0, ret) << "Falls for /hello_world/test" << std::endl;
 ASSERT_EQ("test", attr.file_name) << "Falls for attr.file_name" << std::endl;
}

TEST_F(TestMetaSqlite, MkDir)
{
 TFileAttr attr;

 int ret = meta->mkdir("/hello_world/test2", S_IFDIR | 0777);
 ASSERT_EQ(0, ret) << meta->dl->errmsg();

 ret = meta->getattr("/hello_world/test2", attr);
 ASSERT_EQ(0, ret) << meta->dl->errmsg();
 ASSERT_EQ("test2", attr.file_name);
 ASSERT_EQ(S_IFDIR | 0777, attr.st.st_mode);
}

TEST_F(TestMetaSqlite, MkNod)
{
 TFileAttr attr;

 int ret = meta->mknod("/hello_world/test3", S_IFREG | 0777, NULL);
 ASSERT_EQ(0, ret);// << sl->errmsg();

 ret = meta->getattr("/hello_world/test3", attr);
 ASSERT_EQ(0, ret);// << sl->errmsg();
 ASSERT_EQ("test3", attr.file_name);
 ASSERT_EQ(S_IFREG | 0777, attr.st.st_mode);
}

TEST_F(TestMetaSqlite, ChMod)
{
 TFileAttr attr;

 int ret = meta->mknod("/hello_world/test3", S_IFREG | 0777, NULL);
 ASSERT_EQ(0, ret);// << sl->errmsg();

 ret = meta->chmod("/hello_world/test3", S_IFREG | 0776);
 ASSERT_EQ(0, ret);// << sl->errmsg();

 ret = meta->getattr("/hello_world/test3", attr);
 ASSERT_EQ(0, ret);// << sl->errmsg();
 ASSERT_EQ("test3", attr.file_name);
 ASSERT_EQ(S_IFREG | 0776, attr.st.st_mode);
}

TEST_F(TestMetaSqlite, RmDir)
{
 TFileAttr attr;

 int ret = meta->mkdir("/hello_world/test2", S_IFDIR | 0777);
 ASSERT_EQ(0, ret) << meta->dl->errmsg();

 ret = meta->getattr("/hello_world/test2", attr);
 ASSERT_EQ(0, ret) << meta->dl->errmsg();
 ASSERT_EQ("test2", attr.file_name);
 ASSERT_EQ(S_IFDIR | 0777, attr.st.st_mode);

// ret = meta->mknod("/hello_world/test2/test3", S_IFREG | 0777, NULL);
// ASSERT_EQ(0, ret);// << sl->errmsg();

 ret = meta->rmdir("/hello_world/test2");
 ASSERT_EQ(0, ret);// << sl->errmsg();

 int id = meta->dl->path2id("/hello_world/test2");
 ASSERT_EQ(-1, id);// << sl->errmsg();
}

class TestContentSqlite : public TestMetaSqlite
{
 public:
  TestContentSqlite();// { /* init protected members here */ }
 ~TestContentSqlite();// { /* free protected members here */ }
// void SetUp();// { /* called before every test */ }
// void TearDown();// { /* called after every test */ }

 protected:
  TsqliteContent * content;
};

TestContentSqlite::TestContentSqlite()
{
 content = new TsqliteContent();
 content->dl = meta->dl;
}

TestContentSqlite::~TestContentSqlite()
{
 delete(content);
}

TEST_F(TestContentSqlite, Open)
{
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = content->dl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << content->dl->errmsg();

 int ret = content->dl->getId("/hello_world/nodeiu");
 ASSERT_TRUE(ret > 0);

 struct fuse_file_info fi;
 memset(&fi, 0, sizeof(struct fuse_file_info));
 fi.flags = O_RDONLY;
 ret = content->open("/hello_world/nodeiu", &fi);
 ASSERT_EQ(0, ret);

 ASSERT_EQ(1, content->openedFile.size());

 ASSERT_EQ("/hello_world/nodeiu", content->openedFile.begin()->first);
}

TEST_F(TestContentSqlite, Release)
{
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = content->dl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << content->dl->errmsg();

 int ret = content->dl->getId("/hello_world/nodeiu");
 ASSERT_TRUE(ret > 0);

 struct fuse_file_info fi;
 memset(&fi, 0, sizeof(struct fuse_file_info));
 fi.flags = O_RDONLY;
 ret = content->open("/hello_world/nodeiu", &fi);
 ASSERT_EQ(0, ret) << "open error: " << content->dl->errmsg() << std::endl;

 ASSERT_EQ(1, content->openedFile.size());

 ASSERT_EQ("/hello_world/nodeiu", content->openedFile.begin()->first);

 ret = content->release("/hello_world/nodeiu", NULL);
 ASSERT_EQ(0, ret);
 ASSERT_EQ(0, content->openedFile.size());
}

TEST_F(TestContentSqlite, Read)
{
 struct fuse_file_info fi;
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = content->dl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << content->dl->errmsg();

 int ret = content->dl->path2id("/hello_world/nodeiu");
 ASSERT_TRUE(ret > 0);
 ASSERT_EQ(id, ret);

 memset(&fi, 0, sizeof(struct fuse_file_info));
 fi.flags = O_RDWR;
 ret = content->open("/hello_world/nodeiu", &fi);
 ASSERT_EQ(0, ret) << "open error: " << content->dl->errmsg() << std::endl;

 const int BuffSize = TOpenedFile::NodeSize * 5 - TOpenedFile::NodeSize / 2;
 unsigned char * data;
 data = new unsigned char [BuffSize];
 for(int i = 0; i + sizeof(int) < BuffSize; i += sizeof(int))
 {memcpy(data + i, &i, sizeof(int));}
 ret = content->write("/hello_world/nodeiu", (const char *) data, BuffSize, 0, NULL);
 ASSERT_EQ(BuffSize, ret);
 ret = content->release("/hello_world/nodeiu", NULL);
 ASSERT_EQ(0, ret) << "file not release /hello_world/nodeiu" << std::endl;

 TFileList lst;
 content->dl->file_s(lst, id);
 ASSERT_EQ(1, lst.size());
 ASSERT_EQ(BuffSize, lst.front()->st.st_size);
 ASSERT_EQ(id, lst.front()->st.st_ino);

 fi.flags = O_RDONLY;
 ret = content->open("/hello_world/nodeiu", &fi);
 ASSERT_EQ(0, ret) << "file not open /hello_world/nodeiu" << std::endl
                   << content->dl->errmsg() << std::endl;
 memset(data, 0, BuffSize);

 ret = content->read("/hello_world/nodeiu", (char *) data, BuffSize, 0, NULL);
 ASSERT_EQ(BuffSize, ret);
 for(int i = 0; i + sizeof(int) < BuffSize; i += sizeof(int))
 {
  int v(0);
  memcpy(&v, data + i, sizeof(int));
  ASSERT_EQ(i, v);
//  if (i != v)
//   std::cout << "i=" << i <<  " v=" << v << "; ";
 }
// std::cout << std::endl;

 delete [] data;

 TNodeList lst2;
 ret = content->dl->node_s(lst2, id, 0, 0);

// std::cout << "id=" << id << " lst2.size=" << lst2.size() << std::endl;

 /*for(TNodeList::iterator itr = lst2.begin(); itr != lst2.end(); ++itr)
 {
  (*itr)->set_position(0);
  for(unsigned int x = 0; x + sizeof(x) < (*itr)->get_size(); x += sizeof(x))
  {
   int v(0);
   size_t r = (*itr)->read(&v, sizeof(v));
   std::cout << "r=" << r << " v=" << v << "; ";
  }
  break;
 }
 std::cout << std::endl;*/

 ret = content->release("/hello_world/nodeiu", NULL);
 ASSERT_EQ(0, ret);
 ASSERT_EQ(0, content->openedFile.size());
}

TEST_F(TestContentSqlite, Write)
{
 struct fuse_file_info fi;
 fi.flags = O_RDWR | O_CREAT;
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = content->dl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << content->dl->errmsg();

 int ret = content->dl->path2id("/hello_world/nodeiu");
 ASSERT_TRUE(ret > 0);

 ret = content->open("/hello_world/nodeiu", &fi);
 ASSERT_EQ(0, ret) << "open error";

 const int BuffSize = TOpenedFile::NodeSize * 5;
 unsigned char * data;
 data = new unsigned char [BuffSize];

 for(int i = 0; i + sizeof(int) <= BuffSize; i += sizeof(int))
 {memcpy(data + i, &i, sizeof(int));}
 ret = content->write("/hello_world/nodeiu", (const char *) data, BuffSize, 0, NULL);

 delete [] data;
 ASSERT_EQ(BuffSize, ret);

 ret = content->release("/hello_world/nodeiu", NULL);
 ASSERT_EQ(0, ret);
 ASSERT_EQ(0, content->openedFile.size());
}

TEST_F(TestContentSqlite, Truncate)
{
 struct fuse_file_info fi;
 fi.flags = O_RDWR | O_CREAT;
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = content->dl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << content->dl->errmsg();

 int ret = content->dl->path2id("/hello_world/nodeiu");
 ASSERT_EQ(id, ret);

 ret = content->open("/hello_world/nodeiu", &fi);
 ASSERT_EQ(0, ret) << "open error";

 const int BuffSize = TOpenedFile::NodeSize * 5;
 unsigned char * data;
 data = new unsigned char [BuffSize];

 for(int i = 0; i + sizeof(int) <= BuffSize; i += sizeof(int))
 {memcpy(data + i, &i, sizeof(int));}
 ret = content->write("/hello_world/nodeiu", (const char *) data, BuffSize, 0, NULL);

 delete [] data;
 ASSERT_EQ(BuffSize, ret);

 ret = content->release("/hello_world/nodeiu", NULL);
 ASSERT_EQ(0, ret);

 TFileList lst;
 content->dl->file_s(lst, id);
 ASSERT_EQ(1, lst.size());
 ASSERT_EQ(BuffSize, lst.front()->st.st_size);

 int new_size = BuffSize + TOpenedFile::NodeSize / 2;

 ret = content->truncate("/hello_world/nodeiu", new_size);
 ASSERT_EQ(0, ret);

 lst.clear();
 content->dl->file_s(lst, id);
 ASSERT_EQ(1, lst.size());
 ASSERT_EQ(new_size, lst.front()->st.st_size);

 new_size = BuffSize / 2;

 ret = content->truncate("/hello_world/nodeiu", new_size);
 ASSERT_EQ(0, ret);

 lst.clear();
 content->dl->file_s(lst, id);
 ASSERT_EQ(1, lst.size());
 ASSERT_EQ(new_size, lst.front()->st.st_size);
}

TEST_F(TestContentSqlite, Unlink)
{
 struct fuse_file_info fi;
 fi.flags = O_RDWR | O_CREAT;
 TFileAttr attr;
 attr.file_name = "nodeiu";
 attr.st.st_mode = S_IFREG | 0644;
 int id = content->dl->file_iu(1, attr);

 ASSERT_TRUE(id > 0) << content->dl->errmsg();

 int ret = content->dl->path2id("/hello_world/nodeiu");
 ASSERT_EQ(id, ret);

 ret = content->open("/hello_world/nodeiu", &fi);
 ASSERT_EQ(0, ret) << "open error";

 const int BuffSize = TOpenedFile::NodeSize * 5;
 unsigned char * data;
 data = new unsigned char [BuffSize];

 for(int i = 0; i + sizeof(int) <= BuffSize; i += sizeof(int))
 {memcpy(data + i, &i, sizeof(int));}
 ret = content->write("/hello_world/nodeiu", (const char *) data, BuffSize, 0, NULL);

 delete [] data;
 ASSERT_EQ(BuffSize, ret);

 ret = content->release("/hello_world/nodeiu", NULL);
 ASSERT_EQ(0, ret);

 TFileList lst;
 content->dl->file_s(lst, id);
 ASSERT_EQ(1, lst.size());
 ASSERT_EQ(BuffSize, lst.front()->st.st_size);

 content->unlink("/hello_world/nodeiu");
 ret = content->dl->path2id("/hello_world/nodeiu");
 ASSERT_EQ(-1, ret);
}
#endif

