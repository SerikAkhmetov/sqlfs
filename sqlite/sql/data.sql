insert into file(file_id, parent, name, mode, atime, mtime, ctime, uid, gid)
values(1, NULL, 'hello_world', 16384, current_timestamp, current_timestamp, current_timestamp, 0, 0);
insert into file(file_id, parent, name, mode, atime, mtime, ctime, uid, gid)
values(2, 1, 'test', 16384, current_timestamp, current_timestamp, current_timestamp, 0, 0);
insert into file(file_id, parent, name, mode, atime, mtime, ctime, uid, gid)
values(3, NULL, 'foo', 16384, current_timestamp, current_timestamp, current_timestamp, 0, 0);
insert into file(file_id, parent, name, mode, atime, mtime, ctime, uid, gid)
values(4, 3, 'bar', 16384, current_timestamp, current_timestamp, current_timestamp, 0, 0);
