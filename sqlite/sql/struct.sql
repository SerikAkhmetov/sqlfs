--
-- Файл сгенерирован с помощью SQLiteStudio v3.1.0 в Пт дек 7 17:48:07 2018
--
-- Использованная кодировка текста: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: file
CREATE TABLE file (file_id INTEGER PRIMARY KEY NOT NULL, parent INTEGER REFERENCES file (file_id), name VARCHAR (128) NOT NULL, mode INTEGER NOT NULL, atime DATETIME NOT NULL, mtime DATETIME NOT NULL, ctime DATETIME NOT NULL, uid INTEGER NOT NULL, gid INTEGER NOT NULL, size BIGINT);

-- Таблица: node
CREATE TABLE node (node_id INTEGER PRIMARY KEY NOT NULL, file_id INTEGER NOT NULL REFERENCES file (file_id), off BIGINT NOT NULL, size INTEGER NOT NULL, content BLOB);

-- Индекс: k_node_file
CREATE INDEX k_node_file ON node (file_id, off);

-- Индекс: k_file_id
CREATE INDEX k_file_id ON file (file_id);

-- Индекс: k_parent
CREATE UNIQUE INDEX k_parent ON file (parent, name);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;

