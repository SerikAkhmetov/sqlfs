#ifndef DATALAYER_H
#define DATALAYER_H

#include <string>
#include <map>
#include "sqlite3.h"

#include "sqlfs.h"
//-----------------------------------------------------------------------
class TSQLayer
{
 public:
  TSQLayer();
  virtual ~TSQLayer();
  int open();
  bool isOpen();
  void close();

 protected:
  int setQuery(const std::string&);
//  int setQuery(const std::string&, sqlite3_stmt * st);
  //  int exec(Wt::WAbstractItemModel * mt);
  bool exec();
//  bool exec(sqlite3_stmt * st);
  int setParam(const std::string& name, const std::string& v);
  int setParam(const std::string& name, const int v);
  int setParam(const std::string& name, const int64_t v);
  int setParam(const std::string& name, const void* buff, const int length);
  int setParamNull(const std::string& name);

//  /// transaction start
  int begin(void);
//  /// commit
  int commit(void);
//  /// rollback
  void rollback(void);

 public:

  std::string errmsg();
  std::string dbname;

  int file_s(TFileList &lst,const int file_id, const int parent_id = 0);
  int file_iu(const int parent_id, TFileAttr &attr);
  int file_d(const int file_id);
  int node_s(TNodeList & lst, const int file_id, const int64_t off, const int size);
  int node_iu(TNodeList & lst, const int file_id);
  int node_iu(TNode * node, const int file_id);
  int node_d(const int file_id, const int node_id = 0);

  const int path2id(const std::string &path);
  const int path2id(const std::string &path, TFileAttr &attr);

 protected:
  sqlite3 * db;
  sqlite3_stmt * st;
  int trN;
//  std::string sqlquery;
  char *zErrMsg;

//  int st_to_mt(Wt::WAbstractItemModel * mt, sqlite3_stmt *) const;

  int create_struct();
//  int check_version();
  std::string read_file(const std::string& filename);

  int name_length;
//  void init_length();

  const std::string timet_to_a(const time_t * tt);

 private:
   pthread_mutex_t mtx;
   std::map<std::string, int> nameid; // name, id

 public:
   const int getId(const std::string& name);

// private:
//   boost::mutex state_mtx;
//   std::map<int, TState> state_lst; // event_id
//   void push_state(const TState * state, const int event_id);
//   void thread();
//   void stop();
//   bool running;
//   boost::thread thr;

// public:
//   void start();
};
//-----------------------------------------------------------------------

#endif // DATALAYER_H
