#ifndef OPENEDFILE_H
#define OPENEDFILE_H

#include <list>

#include "tbytea.h"
#include "datalayer.h"
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class TOpenedFile
{
public:
 TOpenedFile(TSQLayer * dl);
virtual ~TOpenedFile();

 static const unsigned int NodeSize = 8 * 1024;
 static void clearNodeList(TNodeList & lst);

 TFileAttr attr;
 int mode;
 TNodeList lst_node;
 TSQLayer * dl;

 bool init();
 int read(char *buf, size_t size, off_t offset);
 int write(const char *buf, size_t size, off_t offset);
 bool save();
 bool isModified();
 off_t size();
};
//---------------------------------------------------------------------------
#endif // OPENEDFILE_H
