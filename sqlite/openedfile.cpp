#ifdef TEST_BUILD
#include <gtest/gtest.h>
#include <iostream>
#endif

#include "openedfile.h"

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TOpenedFile::TOpenedFile(TSQLayer * pdl) : mode(0)
{
 dl = pdl;
}
TOpenedFile::~TOpenedFile()
{
 clearNodeList(lst_node);
}
void TOpenedFile::clearNodeList(TNodeList & lst)
{
 for(TNodeList::iterator itr = lst.begin(); itr != lst.end(); ++itr)
 {
  delete(*itr);
 }
 lst.clear();
}
bool TOpenedFile::init()
{
 clearNodeList(lst_node);
 dl->node_s(lst_node, attr.st.st_ino, 0, 0);
 return(true);
}
int TOpenedFile::read(char *buf, size_t size, off_t offset)
{
 int64_t p(0);
 for(std::list<TNode *>::iterator itr = lst_node.begin(); itr != lst_node.end(); ++itr)
 {
  if ((*itr)->off + (*itr)->get_size() < offset) continue;
  if ((*itr)->off > offset + size) break;
  if (p == size) break;

  (*itr)->set_position(offset - (*itr)->off + p);
  int s = (*itr)->get_size() - (*itr)->get_position();
  if (offset + size < (*itr)->off + s)
   s = (*itr)->off + s - (offset + size);
  (*itr)->read(buf + p, s);
//  std::cout << "write off=" << (*itr)->off << " p=" << p << " s = " << s << std::endl;
  p += s;
 }
 return(p);
}

int TOpenedFile::write(const char *buf, size_t size, off_t offset)
{
 size_t p(0);

 if (size == 0) return(0);

 while(lst_node.empty() ||
       lst_node.back()->off + lst_node.back()->get_size() < offset + size)
 {
  TNode * node = new TNode(TOpenedFile::NodeSize);
  if (lst_node.empty()) { node->off = 0; }
  else { node->off = p; }
  p += node->get_size();
  lst_node.push_back(node);
 }

 p = 0;
 for(std::list<TNode *>::iterator itr = lst_node.begin(); itr != lst_node.end(); ++itr)
 {
  if (p == size) break;
  if ((*itr)->off + (*itr)->get_size() < offset) continue;

  (*itr)->set_position(offset - (*itr)->off + p);

  int s = (*itr)->get_size() - (*itr)->get_position();
  if (size < s)
   s = size;
  if (offset + size < (*itr)->off + s)
   {s = (*itr)->off + s - (offset + size);}
  (*itr)->write((void *)(buf + p), s);
  if ((*itr)->usize < (*itr)->get_position())
    (*itr)->usize = (*itr)->get_position();
  (*itr)->modified = true;
  p += s;
 }
 return(p);
}

bool TOpenedFile::save()
{
 bool ret(true);
 if (isModified())
 {
  attr.st.st_size = size();
  attr.st.st_mtime = time(NULL);
  ret = dl->node_iu(lst_node, attr.st.st_ino) > 0 &&
        dl->file_iu(attr.parent, attr) > 0;
 }
 return(ret);
}

bool TOpenedFile::isModified()
{
 bool ret(false);
 for(TNodeList::iterator itr = lst_node.begin(); !ret && itr != lst_node.end(); ++itr)
 {
  ret = (*itr)->modified;
 }
 return(ret);
}

off_t TOpenedFile::size()
{
 off_t ret(0);
 for(TNodeList::iterator itr = lst_node.begin(); itr != lst_node.end(); ++itr)
 {
 // ret += (*itr)->get_size();
  ret += (*itr)->usize;
 }
 return(ret);
}
//---------------------------------------------------------------------------

#ifdef TEST_BUILD

class TestOpenedFile : public ::testing::Test
{
 public:
  TestOpenedFile();// { /* init protected members here */ }
 ~TestOpenedFile();// { /* free protected members here */ }
// void SetUp();// { /* called before every test */ }
// void TearDown();// { /* called after every test */ }

 protected:
  TOpenedFile * of;
};

TestOpenedFile::TestOpenedFile()
{
 of = new TOpenedFile(NULL);
}

TestOpenedFile::~TestOpenedFile()
{
 delete(of);
}

TEST_F(TestOpenedFile, Write)
{
 const int BuffSize = 8 * 1024 * 1024;
 unsigned char * data;
 data = new unsigned char [BuffSize];
 for(int i = 0; i + sizeof(int) < BuffSize; i += sizeof(int))
 {memcpy(data + i, &i, sizeof(int));}
 int ret = of->write((const char *) data, BuffSize, 0);

 ASSERT_EQ(BuffSize, ret) << "ret is false" << std::endl;
 ASSERT_EQ(BuffSize, of->size());

 delete [] data;
}

#endif
